import random
import sys
from PyQt5.QtWidgets import QApplication, QWidget
from window import Ui_Form
import math


RANGE_START = 0
RANGE_END = 100

EASY = 1
MEDIUM = 2
HARD = 3


class MainWindow(QWidget, Ui_Form):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)

        self.mystery_number = None
        self.attempts = 0
        self.difficulty = EASY
        self.attempts_limit = math.inf
        self.game_ended = False

        self.guess_button.clicked.connect(self.on_guess_button_connect)
        self.restart_button.clicked.connect(self.on_restart)
        self.input_number.valueChanged.connect(self.on_value_changed)
        self.easy_radio_button.toggled.connect(self.on_easy_selected)
        self.medium_radio_button.toggled.connect(self.on_medium_selected)
        self.hard_radio_button.toggled.connect(self.on_hard_selected)

        self.easy_radio_button.toggle()

        self.initialize()

    def initialize(self):
        self.attempts = 0
        self.result_value_label.setText("")
        self.attempts_value_label.setText(str(self.attempts))
        self.input_number.setValue(0)
        self.game_ended = False

        self.mystery_number = random.randrange(RANGE_START, RANGE_END)

    def on_guess_button_connect(self):
        self.check_if_game_ended()

        if self.game_ended:
            self.display("Gra zakończyła się")
        else:
            entered_number = self.input_number.value()
            self.attempts += 1

            if entered_number == self.mystery_number:
                self.display(f"Hurra! trafiłeś! Udało Ci się to w {self.attempts} próbach")
                self.game_ended = True
            else:
                if entered_number > self.mystery_number:
                    self.display("Pudło! Wylosowana liczba jest mniejsza!")
                else:
                    self.display("Pudło! Wylosowana liczba jest większa!")

            self.attempts_value_label.setText(str(self.attempts))
            if self.difficulty != EASY:
                self.attempts_left_label.setText(str(self.attempts_limit - self.attempts))
            else:
                self.attempts_left_label.setText("Bez ograniczeń")

    def on_restart(self):
        self.initialize()

    def on_value_changed(self):
        self.result_value_label.setText("")

    def on_easy_selected(self):
        self.difficulty = EASY
        self.attempts_limit = math.inf
        self.attempts_left_label.setText("Bez ograniczeń")

    def on_medium_selected(self):
        self.difficulty = MEDIUM
        self.attempts_limit = 10
        self.attempts_left_label.setText(str(self.attempts_limit))

    def on_hard_selected(self):
        self.difficulty = HARD
        self.attempts_limit = 5
        self.attempts_left_label.setText(str(self.attempts_limit))

    def check_if_game_ended(self):
        if self.attempts >= self.attempts_limit or self.game_ended:
            self.game_ended = True
            return True
        else:
            self.game_ended = False
            return False

    def display(self, msg):
        self.result_value_label.setText(msg)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    x = MainWindow()
    x.show()

    sys.exit(app.exec_())
